require('colors')

const yargs = require('yargs')
const chalk = require('chalk')
const clear = require('clear')
const figlet = require('figlet')
const sh = require('sh-exec')
const path = require('path')
const sander = require('sander')

module.exports.printHeader = () => {
    clear()
    console.log(
        chalk.red(
            figlet.textSync('S A V O I E', {}) // horizontalLayout: 'full'
        )
    )
}

module.exports.getArgv = () => {
    return yargs
        .command('db <operation> [operationParam]', 'db Operation', function (
            yargs
        ) {
            return yargs
                .option('d', {
                    alias: 'dbname',
                    describe: 'Specify a database name',
                })
                .option('f', {
                    alias: 'force',
                    describe: 'Force an action',
                })
                .option('u', {
                    alias: 'username',
                    describe: 'Set an username/email',
                })
                .option('p', {
                    alias: 'password',
                    describe: 'Set a password',
                })
        })

        .command('addModule <moduleName>', 'Mark a folder as module (<moduleName> should be a folder inside ./apps)',yargs=>{
            return yargs.option('db', {
                alias: 'dbname',
                describe: 'Specify a database name',
                default:''
            })
        })

        .help().argv
}

module.exports.execute = async (mainCommand, args) => {


    const defaultDbName = args.dbname || process.env.MYSQL_DATABASE || 'savoie'

    const app = require('express')
    require('./server/functions')(app)

    await app.loadApiFunctions({
        path: require('path').join(__dirname, '/server/funql_api'),
        scope: {
            dbName: args.dbname || process.env.MYSQL_DATABASE || 'savoie',
        },
    })

    const debug = app.getDebugInstance('savoie-cli')

    debug({
        mainCommand,
        args
    })

    if (mainCommand === 'addModule') {

        let outerResult = {
            result:{}
        }
        let result = (await app.dbExecute(`INSERT INTO modules (title, enabled, db_name)values(?,1,?)`, [args.moduleName, args.dbname], {
            defaultDbName,
        },outerResult))[0]

        debug({
            result,
            outerResult
        })
        
    }

    if (mainCommand === 'db') {


        if (args.operation === 'check') {
            
            try {
                await app.dbExecute(`show databases`, [], {})
                debug(`Connected`.green)
            } catch (err) {
                debug(`Not connected`.red)
            }
        }

        if (args.operation === 'tables') {
            
            let dbName = args.dbname || process.env.MYSQL_DATABASE || 'savoie'
            try {
                let tables = await app.dbExecute(`show tables`, [], {
                    dbName,
                })
                debug(`Tables in ${dbName}`.green)
                debug(tables.map(r => r[Object.keys(r)[0]]))
            } catch (err) {
                debug({
                    err: err.red,
                })
            }
        }

        //e.g bunx savoie db checkUsrPwd --u pepe@fake.com --p pepe
        if (args.operation === 'checkUsrPwd') {
            
            try {
                let user = (await app.dbExecute(`select * from users where email = ?`, [args.username], {
                    defaultDbName,
                }))[0]
                if (!user) {
                    debug('User not found')
                } else {
                    debug(`Users`.red)
                    const isPasswordVerified = await Bun.password.verify(args.password, user.password);
                    debug({

                    })
                    debug({
                        user,
                        args,
                        isPasswordVerified
                    })
                }
            } catch (err) {
                debug({
                    err: err,
                })
            }

        }

        //e.g bunx savoie db createUser --email=pepe@fake.com --username=pepe --password=pepe
        if (args.operation === 'createUser') {
            
            let dbName = args.dbname || process.env.MYSQL_DATABASE || 'savoie'
            try {
                let r = await app.api.common_create_account({
                    email: args.email || args.username,
                    password: args.password,
                })
                debug(r)
                debug(`done`.green)
            } catch (err) {
                debug({
                    err: err.stack.red,
                })
            }
        }

        if (args.operation === 'list') {
            
            let dbName = args.dbname || process.env.MYSQL_DATABASE || 'savoie'
            try {
                let query = `show databases`
                if (['modules', 'users', 'feedbacks'].includes(args.operationParam)) {
                    query = `SELECT * FROM ${args.operationParam}`
                }
                let r = await app.dbExecute(query, [], {
                    dbName,
                })
                debug(r)
            } catch (err) {
                debug({
                    err: err.stack.red,
                })
            }
        }
        if (args.operation === 'populate') {
            
            try {
                let dbName = args.dbname || process.env.MYSQL_DATABASE || 'savoie'

                let answers = await require('inquirer').prompt([{
                    type: 'confirm',
                    name: 'confirm',
                    default: true,
                    message: `Populate existing database '${dbName}' ? (db must be empty)`,
                },])

                if (!answers.confirm) {
                    console.log('Aborting..')
                    process.exit(0)
                }

                let r = []
                try {
                    r = await app.dbExecute(`show tables`, [], {
                        dbName,
                    })
                } catch (err) { }

                if (r.length > 0 && !args.force) {
                    debug(`Database ${dbName} is not empty`.red)
                    debug(`Try running with --dbname=newDatabase or --force`.yellow)
                } else {
                    debug(`${dbName}`.green)
                    debug(`Generating schemes`)
                    let initialMigrationPath = path.join(
                        __dirname,
                        './migrations/initial.sql'
                    )
                    let initialMigrationSQL = (await sander.readFile(
                        initialMigrationPath
                    )).toString('utf-8')
                    initialMigrationSQL = initialMigrationSQL
                        .split(`__DATABASE_NAME__`)
                        .join(dbName)
                    let conn = await app.getMysqlConnection({})
                    await conn.query(initialMigrationSQL)
                    conn.release()
                    debug(`Database ${dbName} is ready to use`.green)
                }
            } catch (err) {
                debug({
                    err,
                })
            }
        }
        
    }
    process.exit(0)
}