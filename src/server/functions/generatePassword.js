module.exports = app => {
  return async function generatePassword (newPassword) {
    var debug = require('debug')(`app:generatePassword ${`${Date.now()}`.white}`)
    const pwd = await app.hashPwd(newPassword)
    debug({pwd})
    return pwd
  }
}